package database

import (
	"database/sql"
	_ "github.com/lib/pq"
	"magister/config"
	"magister/data"
	//"log"
)

type Database struct {
	ConnectionString string
	Driver           string
	db               *sql.DB
}

func (db *Database) Connect(connectionString, driverName string) error {
	tmp, err := sql.Open(driverName, connectionString)
	db.db = tmp
	if err != nil {
		config.GetConfig().DataBaseLog.Println(err)
	} else {
		config.GetConfig().DataBaseLog.Println("database connect")
	}
	return err
}

func (db *Database) Close() error {
	return db.db.Close()
}

func (db *Database) GetCursor() (*sql.Tx, error) {
	cur, err := db.db.Begin()
	return cur, err
}

func (db *Database) LoadCnf(name string) (string, error) {
	row := db.db.QueryRow("select data from config where name=$1", name)
	var val string
	err := row.Scan(&val)
	if err != nil {
		config.GetConfig().DataBaseLog.Println(err)
	}
	return val, err
}
func (db *Database) SaveCnf(name, data string) error {
	_, err := db.db.Exec("update config set data=$1 where name=$2", data, name)
	return err
}
func (db *Database) NewNode(node *data.Node) error {
	_, err := db.db.Exec("insert into Node (id, name, type) values ($1,$2,$3)", node.Id, node.Name, node.Type)
	return err
}

func (db *Database) NewLink(link *data.Link) error {
	_, err := db.db.Exec("insert into Link (id, name, type, node1_id, node2_id) values ($1,$2,$3,$4,$5)", link.Id, link.Name, link.Type, link.Node1.Id, link.Node2.Id)
	return err
}

func (db *Database) NewGraph(graph *data.Graph) error {
	_, err := db.db.Exec("insert into Graph (id, name, type) values ($1,$2,$3)", graph.Id, graph.Name, graph.Type)
	return err
}

func (db *Database) NewConnNodetoGraph(node *data.Node, graph *data.Graph) error {
	_, err := db.db.Exec("insert into N2GLT (node_id, graph_id) values ($1,$2)", node.Id, graph.Id)
	return err
}

func (db *Database) NewConnLinktoGraph(link *data.Link, graph *data.Graph) error {
	_, err := db.db.Exec("insert into L2GLT (link_id, graph_id) values ($1,$2)", link.Id, graph.Id)
	return err
}
