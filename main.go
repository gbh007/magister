package main

import (
	//"database/sql"
	"fmt"
	//_ "github.com/lib/pq"
	"magister/data"
	"magister/database"
	"strconv"
	"log"
	"magister/config"
)
func le(err error){
	if err!=nil{
		log.Println(err)
	}
}
func main() {
	config.GetConfig().Init()
	db := new(database.Database)
	db.Connect("user=postgres password=1 dbname=postgres sslmode=disable", "postgres")
	defer db.Close()
	graph := new(data.Graph)
	graph.Id = 0
	graph.Name = "main"
	graph.Type = "muin"
	le(db.NewGraph(graph))
	for i := 0; i < 10; i++ {
		node := new(data.Node)
		node.Id = i
		node.Name = "node " + strconv.Itoa(i)
		node.Type = "node T " + strconv.Itoa(i)
		graph.Nodes = append(graph.Nodes, node)
		le(db.NewNode(node))
		le(db.NewConnNodetoGraph(node, graph))
	}
	ind := 0
	for i := 0; i < 10; i++ {
		for j := i; j < 10; j++ {
			link := new(data.Link)
			link.Id = ind
			link.Name = "link " + strconv.Itoa(ind)
			link.Type = "link T " + strconv.Itoa(ind)
			ind++
			link.Node1 = graph.Nodes[i]
			link.Node2 = graph.Nodes[j]
			graph.Links = append(graph.Links, link)
			le(db.NewLink(link))
			le(db.NewConnLinktoGraph(link, graph))
		}
	}
}

func test1() {
	graph := new(data.Graph)
	graph.Id = 0
	graph.Name = "main"
	graph.Type = "muin"
	for i := 0; i < 10; i++ {
		node := new(data.Node)
		node.Id = i
		node.Name = "node " + strconv.Itoa(i)
		node.Type = "node T " + strconv.Itoa(i)
		graph.Nodes = append(graph.Nodes, node)
	}
	ind := 0
	for i := 0; i < 10; i++ {
		for j := i; j < 10; j++ {
			link := new(data.Link)
			link.Id = ind
			link.Name = "link " + strconv.Itoa(ind)
			link.Type = "link T " + strconv.Itoa(ind)
			ind++
			link.Node1 = graph.Nodes[i]
			link.Node2 = graph.Nodes[j]
			graph.Links = append(graph.Links, link)
		}
	}
	fmt.Println(graph.GetInfoStr())
}
