drop table if exists node,link,graph,n2glt,l2glt,config cascade;

create table if not exists Node(
	id int primary key,
	name text,
	type text
);
create table if not exists Link(
	id int primary key,
	name text,
	type text,
	node1_id int references Node(id),
	node2_id int references Node(id)
);
create table if not exists Graph(
	id int primary key,
	name text,
	type text
);
create table if not exists N2GLT(
	node_id int references Node(id),
	graph_id int references Graph(id)
);
create table if not exists L2GLT(
	link_id int references Link(id),
	graph_id int references Graph(id)
);
create table if not exists Config(
	name text primary key,
	data text
);
insert into Config(name,data) values('LastNodeId','0');
insert into Config(name,data) values('LastLinkId','0');
insert into Config(name,data) values('LastGraphId','0');
insert into Config(name,data) values('LastN2GLTId','0');
insert into Config(name,data) values('LastL2GLTId','0')