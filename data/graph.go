package data

import (
	"bytes"
	"fmt"
)

type Graph struct {
	BaseObject
	Nodes []*Node
	Links []*Link
}

func (g *Graph) GetInfoStr() string {
	buff := bytes.Buffer{}
	for _, e := range g.Nodes {
		_, _ = buff.WriteString(fmt.Sprintln(e.GetInfoStr()))
	}
	for _, e := range g.Links {
		_, _ = buff.WriteString(fmt.Sprintln(e.GetInfoStr()))
	}
	return buff.String()
}
