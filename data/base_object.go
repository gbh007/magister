package data
import "fmt"
type BaseObject struct{
	Id int
	Name string
	Type string
}
func(bo *BaseObject) GetInfoStr() string{
	return fmt.Sprintf(`ID "%d" NAME "%s" TYPE "%s"`, bo.Id, bo.Name, bo.Type)
}