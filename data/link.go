package data
import "fmt"

type Link struct {
	BaseObject
	Node1 *Node
	Node2 *Node
}
func(l *Link) GetInfoStr() string{
	return fmt.Sprintf(`%s NODE1 ID "%d" NODE2 ID "%d"`, l.BaseObject.GetInfoStr(), l.Node1.Id, l.Node2.Id)
}
