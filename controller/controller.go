package controller

import ()

type Controller struct {
	LastNodeId  int
	LastLinkId  int
	LastGraphId int
	LastN2GLTId int
	LastL2GLTId int
}