package config

import (
	"log"
	"os"
	"sync"
	"encoding/json"
)
type ConfigData struct {
	LogFilename string
}
type Config struct {
	CnfData *ConfigData
	DataBaseLog *log.Logger
}

var _config *Config
var _once sync.Once

func (cnf *Config) Init(){
	file, err := os.Open("config.json")
	if err!=nil{
		log.Println(err)
	}
	decoder := json.NewDecoder(file)
	cnf.CnfData = new(ConfigData)
	decoder.Decode(cnf.CnfData)
	logfile, err := os.Create(cnf.CnfData.LogFilename)
	if err!=nil{
		log.Println(err)
	}
	cnf.DataBaseLog = log.New(logfile, "[DB] ", log.Ldate | log.Ltime | log.Lshortfile)
}

func GetConfig() *Config{
	_once.Do(func(){_config = new(Config)})
	return _config 
}